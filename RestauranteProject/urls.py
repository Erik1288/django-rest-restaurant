from django.contrib import admin

from django.urls import path, include

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from rest_framework import routers

from Api import views

router = routers.DefaultRouter()
router.register(r'client', views.ClientViewSet)
router.register(r'waiter', views.WaiterViewSet)
router.register(r'table', views.TableViewSet)
router.register(r'product', views.ProductViewSet)
router.register(r'invoice', views.InvoiceViewSet)
router.register(r'order', views.OrderViewSet)


urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('admin/', admin.site.urls),
    path('', include('Api.urls')),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]
