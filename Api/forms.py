from django import forms

from Api.models import Client, Invoice, Product, Table, Waiter, Order


class ClientForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = ['name', 'last_name', 'observations']


class WaiterForm(forms.ModelForm):
    class Meta:
        model = Waiter
        fields = ['first_name', 'last_name']


class TableForm(forms.ModelForm):
    class Meta:
        model = Table
        fields = ['number_diner', 'location']


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['name', 'description', 'imported']


class InvoiceForm(forms.ModelForm):
    class Meta:
        model = Invoice
        fields = ['date_invoice', 'id_client', 'id_waiter', 'id_table']


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ['quantity', 'products', 'invoices']
