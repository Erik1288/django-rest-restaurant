from django.urls import path

from .views import (ClientCreateView, ClientUpdateView, SuccessCreateClient,
                    ClientListView, ClientDeleteView,
                    )

from .views import(OrderCreateView, OrderDeleteView,
                   OrderListView, OrderUpdateView)

from .views import(InvoiceCreateView, InvoiceDeleteView,
                   InvoiceListView, InvoiceUpdateView,)

from .views import (TableCreateView, TableDeleteView,
                    TableListView, TableUpdateView)

from .views import (WaiterCreateView, WaiterListView,
                    WaiterUpdateView, WaiterDeleteView)

from .views import(ProductCreateView, ProductListView,
                   ProductUpdateView,  ProductDeleteView,)


app_name = 'restaurant_app'
# ------------------ urls client ----------------------------------------
urlpatterns = [
    path('create-client/', ClientCreateView.as_view(), name='creatClient'),
    path('success-client/', SuccessCreateClient.as_view(), name='success'),
    path('list-clients/', ClientListView.as_view(), name='listClient'),
    path('delete-client/<pk>/', ClientDeleteView.as_view(), name='deletClient'),
    path('update-client/<pk>', ClientUpdateView.as_view(), name='updateClient'),

    # -------------------- urls waiter ---------------------------------------

    path('create-waiter/', WaiterCreateView.as_view(), name='createWaiter'),
    path('list-waiter/', WaiterListView.as_view(), name='listWaiter'),
    path('update-waiter/<pk>', WaiterUpdateView.as_view(), name='updateWaiter'),
    path('delete-waiter/<pk>', WaiterDeleteView.as_view(), name='deleteWaiter'),

    # -----------------------urls table ------------------------------------------

    path('create-table/', TableCreateView.as_view(), name='createTable'),
    path('list-table/', TableListView.as_view(), name='listTable'),
    path('update-table/<pk>', TableUpdateView.as_view(), name='updateTable'),
    path('delete-table/<pk>', TableDeleteView.as_view(), name='deleteTable'),

    # ------------------------urls Product -------------------------------------------

    path('create-product/', ProductCreateView.as_view(), name='deleteTable'),
    path('list-product/', ProductListView.as_view(), name='listProduct'),
    path('update-product/<pk>', ProductUpdateView.as_view(), name='updateProduct'),
    path('delete-product/<pk>', ProductDeleteView.as_view(), name='deleteProduct'),

    # ------------------------- urls Invoice ---------------------------------------
    path('create-invoice/', InvoiceCreateView.as_view(), name='createInvoice'),
    path('list-invoice/', InvoiceListView.as_view(), name='listInvoice'),
    path('update-invoice/<pk>', InvoiceUpdateView.as_view(), name='updateInvoice'),
    path('delete-invoice/<pk>', InvoiceDeleteView.as_view(), name='deleteInvoice'),

    # ------------------------- urls Order ---------------------------------------

    path('create-order/', OrderCreateView.as_view(), name='createOrder'),
    path('list-order/', OrderListView.as_view(), name='listOrder'),
    path('update-order/<pk>', OrderUpdateView.as_view(), name='updateOrder'),
    path('delete-order/<pk>', OrderDeleteView.as_view(), name='deleteOrder'),
]
