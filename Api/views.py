from django.urls import reverse_lazy
from django.views.generic import (CreateView, TemplateView, ListView, DeleteView,
                                  UpdateView)

from .models import Client, Waiter, Table, Product, Invoice, Order

from Api.forms import ClientForm, ProductForm, WaiterForm, TableForm, InvoiceForm, OrderForm

from rest_framework import viewsets

from Api.serializers import ClientSerializer, InvoiceSerializer, OrderSerializer, ProductSerializer, TableSerializer, WaiterSerializer


class ClientCreateView(CreateView):
    model = Client
    template_name = "client/create-client.html"
    form_class = ClientForm
    success_url = reverse_lazy('restaurant_app:success')


class SuccessCreateClient(TemplateView):
    template_name = "client/success.html"


class ClientListView(ListView):
    model = Client
    context_object_name = 'list'
    template_name = "client/list-client.html"
    ordering = 'id'


class ClientDeleteView(DeleteView):
    model = Client
    template_name = "client/delete-client.html"
    success_url = reverse_lazy('restaurant_app:success')


class ClientUpdateView(UpdateView):
    model = Client
    template_name = "client/client_update_form.html"
    fields = ['name', 'last_name', 'observations']
    success_url = reverse_lazy('restaurant_app:success')


class ClientViewSet(viewsets.ModelViewSet):
    # class client Serializers
    queryset = Client.objects.all().order_by('id')
    serializer_class = ClientSerializer


# ... views class waiter ...'''

class WaiterCreateView(CreateView):
    model = Waiter
    template_name = "waiter/create-waiter.html"
    form_class = WaiterForm
    success_url = reverse_lazy('restaurant_app:success')


class WaiterListView(ListView):
    model = Waiter
    context_object_name = 'listWaiter'
    template_name = "waiter/list-waiter.html"
    ordering = 'id'


class WaiterUpdateView(UpdateView):
    model = Waiter
    template_name = "waiter/waiter_update_form.html"
    fields = ['first_name', 'last_name']
    success_url = reverse_lazy('restaurant_app:success')


class WaiterDeleteView(DeleteView):
    model = Waiter
    template_name = "waiter/delete-waiter.html"
    success_url = reverse_lazy('restaurant_app:success')


class WaiterViewSet(viewsets.ModelViewSet):
    # class waiter Serializers
    queryset = Waiter.objects.all().order_by('id')
    serializer_class = WaiterSerializer


'''----------- views class Table--------------------'''


class TableCreateView(CreateView):
    model = Table
    template_name = "table/create-table.html"
    form_class = TableForm
    success_url = reverse_lazy('restaurant_app:success')


class TableListView(ListView):
    model = Table
    context_object_name = 'listTable'
    template_name = "table/list-table.html"
    ordering = 'number_diner'


class TableUpdateView(UpdateView):
    model = Table
    template_name = "table/table_update_form.html"
    fields = ['number_diner', 'location']
    success_url = reverse_lazy('restaurant_app:success')


class TableDeleteView(DeleteView):
    model = Table
    template_name = "table/delete-table.html"
    success_url = reverse_lazy('restaurant_app:success')


class TableViewSet(viewsets.ModelViewSet):
    # class Table Serializers
    queryset = Table.objects.all().order_by('id')
    serializer_class = TableSerializer


'''-------------- class views Product-------------------'''


class ProductCreateView(CreateView):
    model = Product
    template_name = "product/create-product.html"
    form_class = ProductForm
    success_url = reverse_lazy('restaurant_app:success')


class ProductListView(ListView):
    model = Product
    context_object_name = 'listProduct'
    template_name = "product/list-product.html"
    ordering = 'name'


class ProductUpdateView(UpdateView):
    model = Product
    template_name = "product/product_update_form.html"
    fields = ['name', 'description', 'imported']
    success_url = reverse_lazy('restaurant_app:success')


class ProductDeleteView(DeleteView):
    model = Product
    template_name = "product/delete-product.html"
    success_url = reverse_lazy('restaurant_app:success')


class ProductViewSet(viewsets.ModelViewSet):
    # class Product Serializers
    queryset = Product.objects.all().order_by('id')
    serializer_class = ProductSerializer

# --------------- views Invoice ---------------------------------


class InvoiceCreateView(CreateView):
    model = Invoice
    template_name = "invoice/create-invoice.html"
    form_class = InvoiceForm
    success_url = reverse_lazy('restaurant_app:success')


class InvoiceListView(ListView):
    model = Invoice
    context_object_name = 'listInvoice'
    template_name = "invoice/list-invoice.html"
    ordering = 'date_invoice'


class InvoiceUpdateView(UpdateView):
    model = Invoice
    template_name = "invoice/invoice_update_form.html"
    fields = ['date_invoice', 'id_client', 'id_waiter', 'id_table']
    success_url = reverse_lazy('restaurant_app:success')


class InvoiceDeleteView(DeleteView):
    model = Invoice
    template_name = "invoice/delete-invoice.html"
    success_url = reverse_lazy('restaurant_app:success')


class InvoiceViewSet(viewsets.ModelViewSet):
    # class Invoice Serializers
    queryset = Invoice.objects.all().order_by('id')
    serializer_class = InvoiceSerializer

# --------------- views Order ------------------------------------------


class OrderCreateView(CreateView):
    model = Order
    template_name = "order/create-order.html"
    form_class = OrderForm
    success_url = reverse_lazy('restaurant_app:success')


class OrderListView(ListView):
    model = Order
    context_object_name = 'listOrder'
    template_name = "order/list-order.html"
    ordering = 'products'


class OrderUpdateView(UpdateView):
    model = Order
    template_name = "order/order_update_form.html"
    fields = ['quantity', 'products', 'invoices']
    success_url = reverse_lazy('restaurant_app:success')


class OrderDeleteView(DeleteView):
    model = Order
    template_name = "order/delete-order.html"
    success_url = reverse_lazy('restaurant_app:success')


class OrderViewSet(viewsets.ModelViewSet):
    # class Order Serializers
    queryset = Order.objects.all().order_by('id')
    serializer_class = OrderSerializer
